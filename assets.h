#pragma once

struct asset
{
	std::string name;
	int id;
	sf::Image image;

	asset(std::string name, std::string dir, int id)
	{
		this->name = name;
		this->id = id;
		image.loadFromFile(dir);
	}
};

struct assetManager
{
	std::vector <asset> stuff;
	int index = 0;
	sf::Image errorImg;

	assetManager()
	{
		errorImg.create(TS, TS, sf::Color(200, 0, 0));
		addDirectory("assets/heroes");
		addDirectory("assets/tiles");
		addDirectory("assets/effects");
		addDirectory("assets/player");
		//listAssets();
	}
	bool isSolid(std::string arg)
	{
		if (arg == "dirt")
			return true;
		if (arg == "stone")
			return true;
		return false;
	}
	sf::Image* get(std::string arg)
	{
		for (asset &entry : stuff)
		{
			if (entry.name == arg)
				return &entry.image;
		}
		return &errorImg;
	}
	void addDirectory(std::string path)
	{
		namespace fs = std::filesystem;
		std::string dir;
		std::string name;
		for (const auto & entry : fs::directory_iterator(path))
		{
			dir = entry.path().string();
			name = dir;
			name.pop_back();
			name.pop_back();
			name.pop_back();
			name.pop_back();
			name.erase(0, path.length() + 1);
			stuff.push_back(*new asset(name, dir, index));
			index++;
		}
	}
	void listAssets()
	{
		for (asset &entry : stuff)
		{
			std::cout << "id=" << entry.id << std::endl;
			std::cout << "name=" << entry.name << std::endl;
		}
	}
};
