#pragma once

class Hero : public actor
{
public:
	sf::Clock clock;
	sf::Time time;
	int var;
	float waitTime = 1;

	Hero()
	{
		type = "deadpool";
	}

	void randomHero()
	{
		int random;
		random = rand() % 17;
		switch (random)
		{
		case 0:
			type = "blackwidow";
			break;
		case 1:
			type = "captainamerica";
			break;
		case 2:
			type = "deadpool";
			break;
		case 3:
			type = "drax";
			break;
		case 4:
			type = "gamora";
			break;
		case 5:
			type = "hawkeye";
			break;
		case 6:
			type = "hulk";
			break;
		case 7:
			type = "janefoster";
			break;
		case 8:
			type = "loki";
			break;
		case 9:
			type = "nebula";
			break;
		case 10:
			type = "nickfury";
			break;
		case 11:
			type = "pepperpotts";
			break;
		case 12:
			type = "rocket";
			break;
		case 13:
			type = "starlord";
			break;
		case 14:
			type = "thor";
			break;
		case 15:
			type = "tonystark";
			break;
		case 16:
			type = "wintersoldier";
			break;
		}
	}
	void act()
	{
		time = clock.getElapsedTime();
			if (time >= sf::seconds(waitTime))
			{
				var = rand() % 8;
				waitTime = (rand() % 10 + 1) / 10;
				if (var >= 0 && var <= 3)
					preDirection = var;
				else
					preDirection = null;
				clock.restart();
			}
	}
};