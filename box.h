#pragma once
#include "assets.h"

extern sf::RenderWindow* root;
extern assetManager* assets;
extern int TS;

class box
{
public:
	std::string type;
	int x;
	int y;
	bool solid;
	sf::Texture texture; // no respond error
	sf::Sprite sprite;

	box(std::string type, int x, int y)
	{
		this->type = type;
		this->x = x;
		this->y = y;
		solid = assets->isSolid(type);
		texture.loadFromImage(*assets->get(type));
		sprite.setTexture(texture);
		sprite.setPosition(x*TS, y*TS);
	}
};
