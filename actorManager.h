#pragma once

namespace actorManager
{
	std::vector <Hero> actors;
	Hero temp;
	int actorId=1;

	void init()
	{
		for (int i = 0; i < 50; i++)
		{
			temp.create(8, 8);
			temp.id = actorId;
			temp.randomHero();
			actors.push_back(temp);
			actorId++;
		}
	}
	void act()
	{
		for (auto &actor : actors)
		{
			actor.act();
			actor.move();
		}
	}
	void draw()
	{
		for (auto &actor : actors)
		{
			actor.draw();
		}
	}
	void getId()
	{
		for (int i = 0; i < actors.size(); i++)
		{
			if (actors[i].getId() != "null")
			{
				hud::idString = actors[i-1].type;
				hud::showType = true;
			}
		}
	}
}