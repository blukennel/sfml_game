#pragma once

extern sf::RenderWindow* root;
extern sf::Event* event;
extern int TS;

namespace config
{
	int resX;
	int resY;
	int fpsMax;
	bool fullscreen;
	int fps;
	sf::Text fpsText;
	sf::String fpsString;
	sf::Font arial;
	sf::Time fpsTime;
	sf::Clock fpsClock;

	void newConfig();
	void loadConfig();

	void init()
	{
		srand(time(NULL));

		std::fstream file;
		file.open("config.conf", std::ios::in);
		if (file.good() == true)
			loadConfig();
		else
			newConfig();
		file.close();

		if (fullscreen)
			root->create(sf::VideoMode(resX, resY, 32),
				"SFML_game",
				sf::Style::Fullscreen);
		else
			root->create(sf::VideoMode(resX, resY, 32),
				"SFML_game");
		root->setFramerateLimit(fpsMax);

		arial.loadFromFile("fonts/arial.ttf");
		fpsText.setCharacterSize(30);
		fpsText.setFillColor(sf::Color(255, 255, 100));
		fpsText.setOutlineColor(sf::Color(0, 0, 0));
		fpsText.setOutlineThickness(3);
		fpsText.setFont(arial);
		fpsText.setPosition(resX-resX/20, 0);
	}
	void newConfig()
	{
		resX = 1366;
		resY = 768;
		fullscreen = false;
		fpsMax = 60;

		std::fstream file;
		std::string data;
		file.open("config.conf", std::ios::out);
		file << "resX=800\n";
		file << "resY=800\n";
		file << "fullscreen=false\n";
		file << "fps=60\n";
		file.close();
	}
	void loadConfig()
	{
		std::fstream file;
		std::string data;
		file.open("config.conf", std::ios::in);

		file.ignore(5);
		getline(file, data);
		resX = stoi(data);

		file.ignore(5);
		getline(file, data);
		resY = stoi(data);

		file.ignore(11);
		getline(file, data);
		if (data == "true")
			fullscreen = true;
		else
			fullscreen = false;

		file.ignore(4);
		getline(file, data);
		fpsMax = stoi(data);

		file.close();
	}
	void getInput()
	{
		if (event->type == sf::Event::Closed)
			root->close();
		if (event->KeyPressed && event->key.code == sf::Keyboard::Escape)
			root->close();
	}
	void getFps()
	{
		fpsTime = fpsClock.getElapsedTime();
		fpsClock.restart();
		fps = 1/fpsTime.asSeconds();
		fpsString = std::to_string(fps);
		fpsText.setString(fpsString);
	}
	void showFps()
	{
		root->draw(fpsText);
	}
}
