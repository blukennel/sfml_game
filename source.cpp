#pragma once
#include "pch.h"
#include "directories.h"

sf::RenderWindow* root;
sf::Event* event;
Player player;
assetManager* assets;
int TS = 32;

int main()
{//========================================================= main start
	sf::RenderWindow rootOrigin;
	root = &rootOrigin;
	sf::Event eventOrigin;
	event = &eventOrigin;
	assetManager assetsOrigin;
	assets = &assetsOrigin;

	config::init();
	Camera::init();
	worldGenerator::generate();
	worldGenerator::generateScene();
	Map::init();
	sceneManager::init();
	actorManager::init();
	hud::init();


	player.create(5, 5);
	effect effect;
	effect.create();

	while (root->isOpen())
	{//====================================================== root start
		player.move();

		actorManager::getId();
		player.getId();
		effect.follow();
		player.cameraFollow();
		actorManager::act();
		config::getFps();

		while (root->pollEvent(*event))
		{//================================================= event start
			config::getInput();
			player.getDirection();
			Camera::getZoom();
		}//================================================= event end

		root->clear();
		Map::draw();
		sceneManager::draw();
		effect.draw();
		actorManager::draw();
		player.draw();


		Camera::setHud();//================================= hud start
		{
			config::showFps();
			hud::draw();
		}
		Camera::setAction();//============================== hud end
		root->display();
	}//===================================================== root end
	return 0;
}//========================================================= main end