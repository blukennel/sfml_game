#pragma once

extern sf::RenderWindow* root;

namespace sceneManager
{
	std::vector <box> scene;

	void init()
	{
		std::string type;
		std::string data;
		int x, y;
		std::fstream file;
		file.open("maps/forest/scene.txt",std::ios::in);
		if (file.good() == true)
		{
			for (int i = 0; i < 10000; i++)
			{
				getline(file, type);
				if (type == "END")
					break;
				getline(file, data);
				x = stoi(data);
				getline(file, data);
				y = stoi(data);

				scene.push_back(*new box(type,x,y));

				if (i == 9999)
					std::cout << "Map corrupted" << std::endl;
			}
			file.close();
		}
		else
			std::cout << "scene.txt missing" << std::endl;
	}
	void draw()
	{
		for (box &entry : scene)
		{
			root->draw(entry.sprite);
		}
	}
}