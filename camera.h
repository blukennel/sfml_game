#pragma once

extern sf::RenderWindow* root;
extern sf::Event* event;
extern int TS;

namespace Camera
{
	sf::View action;
	sf::View hud;
	int zoom;

	void init()
	{
		zoom = 10;
		action.setCenter(sf::Vector2f(TS * 5.f, TS * 5.f));
		action.setSize(sf::Vector2f(TS * 16.f + 1.6*zoom*TS, TS * 9.f + 0.9*zoom*TS));
		hud.reset(sf::FloatRect(0.f, 0.f, config::resX, config::resY));
		root->setView(action);
	}
	void getZoom()
	{
		if (event->type == sf::Event::MouseWheelScrolled)
		{
			if (event->mouseWheelScroll.delta < 0)
				if (zoom >= 0)
				{
					zoom--;
					action.setSize(sf::Vector2f(TS * 16.f + 1.6*zoom*TS, TS * 9.f + 0.9*zoom*TS));
					root->setView(action);
				}
			if (event->mouseWheelScroll.delta >= 0)
				if (zoom <= 20)
				{
					zoom++;
					action.setSize(sf::Vector2f(TS * 16.f + 1.6*zoom*TS, TS * 9.f + 0.9*zoom*TS));
					root->setView(action);
				}
		}
	}
	void setAction()
	{
		root->setView(action);
	}
	void setHud()
	{
		root->setView(hud);
	}
};