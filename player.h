#pragma once

class Player : public actor
{
public:
	Player()
	{
		type = "player";
	}
	void getDirection()
	{
		preDirection = null;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			preDirection = left;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			preDirection = down;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			preDirection = right;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			preDirection = up;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
			speed = 4;
		else
			speed = 2;
	}
	void cameraFollow()
	{
		Camera::action.setCenter(sprite.getPosition());
		root->setView(Camera::action);
	}
};
