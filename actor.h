#pragma once
#include "hud.h"

enum
{
	null = 4,
	right = 2,
	up = 3,
	left = 1,
	down = 0
};

class actor
{
public:
	int id;
	int x;
	int y;
	std::string type;
	float speed;
	int direction;
	int preDirection;
	int lastDirection;
	int animationIndex;
	int animationStep;
	sf::Sprite sprite;
	sf::Texture animation[4][4];

	void convert()
	{
		sf::Image temp;
		sf::Image sheet=*assets->get(type);

		if (sheet.getSize().x == 128 && sheet.getSize().y == 192)
		{
			temp.create(32, 48);
			for (int l = 0; l < 4; l++)
				for (int k = 0; k < 4; k++)
				{
					for (int i = 0; i < 48; i++)
						for (int j = 0; j < 32; j++)
							temp.setPixel(j, i, sheet.getPixel(k * 32 + j, l * 48 + i));
					animation[l][k].loadFromImage(temp);
				}
		}

		if (sheet.getSize().x == 160 && sheet.getSize().y == 224)
		{
			temp.create(40, 56);
			for (int l = 0; l < 4; l++)
				for (int k = 0; k < 4; k++)
				{
					for (int i = 0; i < 56; i++)
						for (int j = 0; j < 40; j++)
							temp.setPixel(j, i, sheet.getPixel(k * 40 + j, l * 56 + i));
					animation[l][k].loadFromImage(temp);
				}
		}

	}
	void create(int x, int y)
	{
		this->x = x;
		this->y = y;
		convert();
		sprite.setTexture(animation[0][0]);
		sprite.setPosition(TS * x, TS * y - TS / 2);
		speed = 2;
		direction = null;
		preDirection = null;
	}
	void move()
	{
		if ((int)sprite.getPosition().x % TS == 0 && (int)sprite.getPosition().y % TS == TS / 2)
		{
			direction = preDirection;
			x = sprite.getPosition().x / TS;
			y = sprite.getPosition().y / TS;
		}

		if (animationIndex >= 32)
			animationIndex = 0;
		if (animationIndex >= 0 && animationIndex <= 7)
			animationStep = 0;
		if (animationIndex >= 8 && animationIndex <= 15)
			animationStep = 1;
		if (animationIndex >= 16 && animationIndex <= 23)
			animationStep = 2;
		if (animationIndex >= 24 && animationIndex <= 31)
			animationStep = 3;

		if (direction == null)
			animationStep = 0;

		switch (direction)
		{
		case right:
			sprite.setTexture(animation[right][animationStep]);
			animationIndex += speed / 2;
			lastDirection = right;
			if(Map::isSolid(x+1,y+1)==false)
				sprite.move(1 * speed, 0);
			break;
		case left:
			sprite.setTexture(animation[left][animationStep]);
			animationIndex += speed / 2;
			lastDirection = left;
			if (Map::isSolid(x-1, y+1) == false)
				sprite.move(-1 * speed, 0);
			break;
		case up:
			sprite.setTexture(animation[up][animationStep]);
			animationIndex += speed / 2;
			lastDirection = up;
			if (Map::isSolid(x, y) == false)
				sprite.move(0, -1 * speed);
			break;
		case down:
			sprite.setTexture(animation[down][animationStep]);
			animationIndex += speed / 2;
			lastDirection = down;
			if (Map::isSolid(x, y+2) == false)
				sprite.move(0, 1 * speed);
			break;
		case null:
			if(lastDirection==left)
				sprite.setTexture(animation[left][animationStep]);
			if (lastDirection == right)
				sprite.setTexture(animation[right][animationStep]);
			if (lastDirection == up)
				sprite.setTexture(animation[up][animationStep]);
			if (lastDirection == down)
				sprite.setTexture(animation[down][animationStep]);
		}
	}
	void draw()
	{
		root->draw(sprite);
	}
	std::string getId()
	{
		sf::Vector2i pixelPos = sf::Mouse::getPosition(*root);
		sf::Vector2f worldPos = root->mapPixelToCoords(pixelPos);
		if (worldPos.x >= sprite.getPosition().x &&
			worldPos.x <= sprite.getPosition().x + TS &&
			worldPos.y >= sprite.getPosition().y &&
			worldPos.y <= sprite.getPosition().y + TS + TS / 2)
		{
			return type;
		}
		return "null";
	}
};
