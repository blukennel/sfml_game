#ifndef PCH_H
#define PCH_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <SFML/Audio.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <filesystem>
#include <time.h>
#include <stdlib.h>

#endif
