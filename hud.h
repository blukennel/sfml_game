#pragma once

namespace hud
{
	bool showType = true;
	sf::String idString="ass";
	sf::Text idText;
	sf::Font arial;

	void init()
	{
		arial.loadFromFile("fonts/arial.ttf");
		idText.setCharacterSize(100);
		idText.setFillColor(sf::Color(0, 200, 0));
		idText.setFont(arial);
		idText.setPosition(0,0);
	}
	void draw()
	{
		idText.setString(idString);
		if (showType)
		{
			root->draw(idText);
			showType = false;
		}

	}
}
