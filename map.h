#pragma once

extern sf::RenderWindow* root;
extern sf::Event* event;
extern int TS;

namespace Map
{
	bool grid[100][100];
	sf::Image image;
	sf::Image temp;
	sf::Sprite sprite;
	sf::Texture texture;

	void init()
	{
		std::fstream file;
		std::string type;
		std::string data;
		int x;
		int y;

		image.create(100*TS, 100*TS, sf::Color(0, 0, 0));

		file.open("forest.map", std::ios::in);
		if (file.good() == true)
		{
			for(int i=0; i<10000; i++)
			{
				getline(file, type);
				if(type=="END")
					break;
				getline(file, data);
				x = stoi(data);
				getline(file, data);
				y = stoi(data);

				if (i == 9999)
					std::cout << "Map corrupted" << std::endl;

				grid[x][y] = assets->isSolid(type);
				temp = *assets->get(type);
				for (int i = 0; i < TS; i++)
				 	for (int j = 0; j < TS; j++)
					{
						image.setPixel(i + x * TS, j + y * TS, temp.getPixel(i, j));
					}
			} 
			file.close();
		}
		else
			std::cout << "Map missing" << std::endl;

		texture.loadFromImage(image);
		sprite.setTexture(texture);
	}
	void draw()
	{
		root->draw(sprite);
	}
	bool isSolid(int x, int y)
	{
		if(grid[x][y]==true)
			return true;
		return false;
	}
}
