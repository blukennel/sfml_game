#pragma once

namespace worldGenerator
{
	void generate()
	{
		std::fstream file;
		std::string data;
		file.open("forest.map", std::ios::out);

		for (int i = 4; i < 47; i++)
			for (int j = 3; j < 47; j++)
			{
				file << "grass" << rand() % 12 << "\n";
				file << i << "\n";
				file << j << "\n";
			}
		
		for (int i = 3; i < 48; i++)
		{
			file << "stone\n";
			file << i << "\n";
			file << 2 << "\n";
		}
		for (int i = 3; i < 48; i++)
		{
			file << "stone\n";
			file << i << "\n";
			file << 47 << "\n";
		}
		for (int i = 2; i < 49; i++)
		{
			file << "stone\n";
			file << i << "\n";
			file << 1 << "\n";
		}
		for (int i = 2; i < 49; i++)
		{
			file << "stone\n";
			file << i << "\n";
			file << 48 << "\n";
		}
		for (int i = 3; i < 48; i++)
		{
			file << "stone\n";
			file << 3 << "\n";
			file << i << "\n";
		}
		for (int i = 3; i < 48; i++)
		{
			file << "stone\n";
			file << 47 << "\n";
			file << i << "\n";
		}
		for (int i = 2; i < 49; i++)
		{
			file << "stone\n";
			file << 2 << "\n";
			file << i << "\n";
		}
		for (int i = 2; i < 49; i++)
		{
			file << "stone\n";
			file << 48 << "\n";
			file << i << "\n";
		}

		file << "END";
	}
	void generateScene()
	{
		std::fstream file;
		std::string data;
		file.open("maps/forest/scene.txt", std::ios::out);

		for (int i = 0; i < 100; i++)
		{
			file << "blood" << rand()%5 <<"\n";
			file << rand()%38+5 << "\n";
			file << rand()%38+5 << "\n";
		}

		file << "END";
	}
}