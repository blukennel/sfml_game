#pragma once

extern assetManager* assets;
extern sf::RenderWindow* root;
extern Player player;

class effect
{
public:
	std::string type;
	sf::Sprite sprite;
	sf::Texture texture;

	void create()
	{
		type = "smoke";
		texture.loadFromImage(*assets->get(type));
		sprite.setTexture(texture);
		disappear();
	}
	void follow()
	{
		if (player.speed == 4)
		{
			if (player.direction == up)
				sprite.setPosition(player.sprite.getPosition().x, player.sprite.getPosition().y+35);
			if (player.direction == down)
				sprite.setPosition(player.sprite.getPosition().x, player.sprite.getPosition().y+TS/2);
			if (player.direction == left)
				sprite.setPosition(player.sprite.getPosition().x + TS/2, player.sprite.getPosition().y+25);
			if (player.direction == right)
				sprite.setPosition(player.sprite.getPosition().x - TS/2, player.sprite.getPosition().y+25);
			if (player.direction == null)
				disappear();
		}
		else
			disappear();
	}
	void disappear()
	{
		sprite.setPosition(-900, -900);
	}
	void draw()
	{
		root->draw(sprite);
	}
};